import json
import requests
import time
import mysql.connector
from decouple import config

# Setup local database connection
mydb = mysql.connector.connect(
  host="localhost",
  user=config('USERNAME'),
  password=config('PASSWORD'),
  database=config('DATABASE')
)
mydb.set_charset_collation('utf8mb4')
mycursor = mydb.cursor()
# Put up bank api key here. Don't have this in the commit.
token = config('TOKEN')
headers = {
 'Authorization': 'Bearer ' + token,
 'Content-Type': 'application/json; charset=utf-8'
 }

accounts = []
transactions = []

# Used to make database select statements
def database_call(sql, value):
  mycursor.execute(sql, value)
  myresult = mycursor.fetchall()
  return myresult

# Used to make database insert statements
def database_commit(sql, value):
    mycursor.executemany(sql, value)
    mydb.commit()

# Check if the user exists in the database already, if not, add them
def check_user():
  myresult = database_call("SELECT * FROM users WHERE idusers =%s;", (token,))
  if myresult == []:
    database_commit("INSERT INTO users (idusers) VALUES (%s)", (token,))
  else:
    print('user exists')

# Create/Update users accounts in database
def update_accounts():
  # Do api call to get current account information
  url = 'https://api.up.com.au/api/v1/accounts'
  response = requests.get(url, headers=headers)
  jsonResponse = response.json()
  # Create accounts array with only needed information to use in insert statement
  for entry in jsonResponse['data']:
    accounts.append([entry['id'], entry['attributes']['displayName'], entry['attributes']['accountType'], entry['attributes']['balance']['value'], token])
  sql = "INSERT INTO accounts (uid, displayName, accountType, balance, idusers) VALUES (%s, %s, %s, %s, %s) ON DUPLICATE KEY UPDATE displayName = VALUES(displayName), accountType = VALUES(accountType), balance = VALUES(balance);"
  # Do Database sql insert call
  database_commit(sql, accounts)
  time.sleep(3)

# Get transaction information for the user
def get_transactions():
  # Check if user has transactions yet.
  myresult = database_call("SELECT * FROM transactions WHERE idusers =%s;", (token,))
  if myresult == []:
    # If user has no transaction data, get all data
    get_all_transactions()
  else:
    # Get only new data
    get_new_transactions()

# Get all transaction data for this user and insert into database
def get_all_transactions():
  loop = False
  # Set url to first page of data
  url = 'https://api.up.com.au/api/v1/transactions?page[size]=100'
  # url will equal None when no data left, until then, loop through pages
  while url != None:
    # Do api call to get transaction data
    response = requests.get(url, headers=headers)
    jsonResponse = response.json()
    # Create transactions array with only needed information to use in insert statement
    for entry in jsonResponse['data']:
      transactions.append([entry['id'], p['relationships']['account']['data']['id'], entry['attributes']['status'], entry['attributes']['rawText'], entry['attributes']['description'], entry['attributes']['message'], convert_json(entry['attributes']['holdInfo']), convert_json(entry['attributes']['roundUp']), convert_json(entry['attributes']['cashback']), convert_json(entry['attributes']['amount']), convert_json(entry['attributes']['foreignAmount']), entry['attributes']['settledAt'], entry['attributes']['createdAt'], token])
    # Set url as the next page, this is included in the data from the up bank api
    url = jsonResponse['links']['next']
    time.sleep(3)
  sql = "INSERT INTO transactions (uid, account_id, status, rawText, description, message, holdInfo, roundUp, cashback, amount, foreignAmount, settledAt, createdAt, idusers) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"
  # Do Database sql insert call
  database_commit(sql, transactions)

# Get only new transaction data for this user and insert into database
def get_new_transactions():
  loop = False
  # Set url to first page of data
  url = 'https://api.up.com.au/api/v1/transactions?page[size]=100'
  sql = "SELECT uid, MAX(createdAt) AS maxdata FROM up_bank.transactions WHERE idusers =%s AND settledAt IS NULL GROUP BY uid ORDER BY createdAt DESC LIMIT 1"
  # Check for the oldest record that has not been settled yet.
  myresult = database_call(sql, (token,))

  # If no transaction is yet to be settled, set mindate as the last transaction date, otherwise set it as the oldest transaction yet to be settled so it can be checked for updates.
  if myresult == []:
    sql = "SELECT uid, MAX(createdAt) AS maxdata FROM up_bank.transactions WHERE idusers =%s GROUP BY uid ORDER BY createdAt DESC LIMIT 1"
    myresult = database_call(sql, (token,))
    minDateId = myresult[0][0]
  else:
    minDateId = myresult[0][0]

  while loop != True:
    # Do api call to get transaction data
    response = requests.get(url, headers=headers)
    jsonResponse = response.json()
    # Create transactions array with only needed information to use in insert statement
    for entry in jsonResponse['data']:
      transactions.append([entry['id'], entry['relationships']['account']['data']['id'], entry['attributes']['status'], entry['attributes']['rawText'], entry['attributes']['description'], entry['attributes']['message'], convert_json(entry['attributes']['holdInfo']), convert_json(entry['attributes']['roundUp']), convert_json(entry['attributes']['cashback']), convert_json(entry['attributes']['amount']), convert_json(entry['attributes']['foreignAmount']), entry['attributes']['settledAt'], entry['attributes']['createdAt'], token])
      # If the current entry has the id as the mindateId, break out of loops
      if entry['id'] == minDateId:
        loop = True
        break
    # Set url as the next page, this is included in the data from the up bank api
    url = jsonResponse['links']['next']
    if url == None or entry['id'] == minDateId:
      loop = True
    time.sleep(3)

  sql = "INSERT INTO transactions (uid, account_id, status, rawText, description, message, holdInfo, roundUp, cashback, amount, foreignAmount, settledAt, createdAt, idusers) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON DUPLICATE KEY UPDATE status = VALUES(status), settledAt = VALUES(settledAt);"
  # Do Database sql insert call
  database_commit(sql, transactions)

# Convert objects to string or null if null value
def convert_json(value):
  if value != None:
    return json.dumps(value)
  else:
    return None
check_user()
update_accounts()
get_transactions()

